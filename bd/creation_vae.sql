CREATE TABLE `CATEGORIE` (
  `idcat` decimal(3,0),
  `nomcat` varchar(50),
  PRIMARY KEY (`idcat`)
);


CREATE TABLE `ENCHERIR` (
  `idut` decimal(6,0),
  `idve` decimal(8,0),
  `dateheure` datetime,
  `montant` decimal(8,2),
  PRIMARY KEY (`idut`, `idve`, `dateheure`),
  FOREIGN KEY (`idve`) REFERENCES `VENTE` (`idve`),
  FOREIGN KEY (`idut`) REFERENCES `UTILISATEUR` (`idut`)
);

CREATE TABLE `OBJET` (
  `idob` decimal(6,0),
  `nomob` varchar(50),
  `descriptionob` text,
  `idut` decimal(6,0),
  `idcat` decimal(3,0),
  PRIMARY KEY (`idob`),
  FOREIGN KEY (`idcat`) REFERENCES `CATEGORIE` (`idcat`)
);

CREATE TABLE `PHOTO` (
  `idph` decimal(6,0),
  `titreph` varchar(50),
  `imgph` blob,
  `idob` decimal(6,0),
  PRIMARY KEY (`idph`),
  FOREIGN KEY (`idob`) REFERENCES `OBJET` (`idob`)
);

CREATE TABLE `ROLE` (
  `idrole` decimal(2,0),
  `nomrole` varchar(30),
  PRIMARY KEY (`idrole`)
);

CREATE TABLE `STATUT` (
  `idst` char,
  `nomst` varchar(30),
  PRIMARY KEY (`idst`)
);

CREATE TABLE `UTILISATEUR` (
  `idut` decimal(6,0),
  `pseudout` varchar(20) unique,
  `emailut` varchar(100),
  `mdput` varchar(100),
  `activeut` char(1),
  `idrole` decimal(2,0),
  PRIMARY KEY (`idut`),
  FOREIGN KEY (`idrole`) REFERENCES `ROLE` (`idrole`)
);

CREATE TABLE `VENTE` (
  `idve` decimal(8,0),
  `prixbase` decimal(8,2),
  `prixmin` decimal(8,2),
  `debutve` datetime,
  `finve` datetime,
  `idob` decimal(6,0),
  `idst` char,
  `nomville` varchar(50),
  PRIMARY KEY (`idve`),
  FOREIGN KEY (`idob`) REFERENCES `OBJET` (`idob`)
);