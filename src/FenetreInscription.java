import javafx.scene.control.Button;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Background;
import javafx.scene.image.Image;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ComboBox;
import javafx.scene.paint.Color;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.image.ImageView;


public class FenetreInscription extends BorderPane {
    
    private HBox hBoxTop;
    private HBox hBoxBottom;
    private VBox vBoxCenter;
    private Label titre;
    private Label mauvaisPseudo;
    private Label mauvaisMail;
    private Label mauvaisMdp;
    private Button boutonRetour;
    private Button inscription;
    private TextField pseudo;
    private TextField mail;
    private TextField mdpAdminInscription;
    private PasswordField mdp;
    private ComboBox<String> role;

    public FenetreInscription(TextField mdpAdminInscription, Label mauvaisPseudo, Label mauvaisMail, Label mauvaisMdp, ComboBox<String> role, TextField pseudo, TextField mail, PasswordField mdp, Button inscription, Button retourMenu){
        StackPane stackPane = new StackPane();
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        this.hBoxTop = new HBox();
        this.hBoxBottom = new HBox();
        this.vBoxCenter = new VBox();
        this.role = role;
        this.inscription = inscription;
        this.boutonRetour = retourMenu;
        this.pseudo = pseudo;
        this.pseudo.clear();
        this.mail = mail;
        this.mail.clear();
        this.mdp = mdp;
        this.mdp.clear();
        this.mdpAdminInscription = mdpAdminInscription;
        this.mdpAdminInscription.clear();
        this.inscription.setStyle("-fx-background-color: #00aef0; -fx-text-fill: black;");
        this.pseudo.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        this.mail.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        this.mdp.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        this.mdpAdminInscription.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        this.pseudo.setPromptText("Entrée votre pseudo");
        this.mail.setPromptText("Entrée votre mail");
        this.mdp.setPromptText("Entrée votre mot de passe");
        this.mdpAdminInscription.setPromptText("Entrée le mot de passe admin");
        this.role.setPromptText("Choisir un rôle");
        this.role.setStyle("-fx-background-color: #012245; -fx-text-fill: #00aef0; -fx-font-size: 14px;");
        this.role.getSelectionModel().select(1);
        ImageView retourMenuIMG = new ImageView(new Image("file:./images/iconRetourMenu.png"));
        retourMenuIMG.setFitHeight(40);
        retourMenuIMG.setFitWidth(40);
        this.boutonRetour.setGraphic(retourMenuIMG);
        this.boutonRetour.setStyle("-fx-background-color: #012245");
        this.titre = new Label("Inscription");
        this.mauvaisPseudo = mauvaisPseudo;
        this.mauvaisMail = mauvaisMail;
        this.mauvaisMdp = mauvaisMdp;
        this.titre.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;"); 
        this.mauvaisPseudo.setStyle("-fx-text-fill: red; -fx-font-size: 14px;");
        this.mauvaisMail.setStyle("-fx-text-fill: red; -fx-font-size: 14px;");
        this.mauvaisMdp.setStyle("-fx-text-fill: red; -fx-font-size: 14px;"); 
        this.mauvaisPseudo.setVisible(false);
        this.mauvaisMail.setVisible(false);
        this.mauvaisMdp.setVisible(false); 
        this.mdpAdminInscription.setVisible(false);                  
        stackPane.getChildren().addAll(this.mauvaisPseudo, this.mauvaisMail, this.mauvaisMdp);
        this.hBoxTop.getChildren().addAll(boutonRetour, titre);
        this.vBoxCenter.getChildren().addAll(role, pseudo, mail, mdp, this.mdpAdminInscription, stackPane);
        this.hBoxBottom.getChildren().addAll(this.inscription);
        this.setTop(this.hBoxTop);
        this.setCenter(this.vBoxCenter);
        this.setBottom(this.hBoxBottom);
        this.titre.setPadding(new Insets(15, 0, 0, 150));  
        this.hBoxBottom.setPadding(new Insets(0, 0, 100, 200));
        this.vBoxCenter.setPadding(new Insets(70, 100, 0, 100));
        this.role.setPadding(new Insets(0, 0, 0, 80));
        this.pseudo.setPadding(new Insets(25, 0, 0, 0));
        this.mail.setPadding(new Insets(25, 0, 0, 0));
        this.mdp.setPadding(new Insets(25, 0, 0, 0));
        this.mdpAdminInscription.setPadding(new Insets(25, 0, 0, 0));
        stackPane.setPadding(new Insets(25, 0, 0, 0));
    }
}
