import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;


public class MenuOptions extends BorderPane {
    
    private Label menu;
    private Label textPageAcceuil;
    private Label textVendreUnProduit;
    private Label textVenteAuxEncheres;
    private Label textMessagerie;
    private Label textMonPanier;
    private Label textInformations;
    private Label textSetting;
    private Label textQuitter;

    public MenuOptions(Label menu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter){
        VBox vBoxTop = new VBox();
        VBox vBoxBottom = new VBox();
        this.menu = menu;
        this.textPageAcceuil = textPageAccueil;
        this.textVendreUnProduit = textVendreUnProduit;
        this.textVenteAuxEncheres = textVenteAuxEncheres;
        this.textMessagerie = textMessagerie;
        this.textMonPanier = textMonPanier;
        this.textInformations = textInformations;
        this.textSetting = textSetting;
        this.textQuitter = textQuitter;
        this.menu.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        this.textPageAcceuil.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        this.textVendreUnProduit.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        this.textVenteAuxEncheres.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        this.textMessagerie.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        this.textMonPanier.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        this.textInformations.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        this.textSetting.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        this.textQuitter.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        vBoxTop.getChildren().addAll(this.menu, this.textPageAcceuil, this.textMessagerie, this.textVenteAuxEncheres, this.textMonPanier, this.textVendreUnProduit);
        vBoxBottom.getChildren().addAll(this.textInformations, this.textSetting, this.textQuitter);
        this.setTop(vBoxTop);
        this.setBottom(vBoxBottom);
        this.textVendreUnProduit.setPadding(new Insets(22,0,0,0));
        this.textVenteAuxEncheres.setPadding(new Insets(22,0,0,0));
        this.textMessagerie.setPadding(new Insets(22,0,0,0));
        this.textMonPanier.setPadding(new Insets(22,0,0,0));
        this.textInformations.setPadding(new Insets(435,0,0,0));
        this.textSetting.setPadding(new Insets(22,0,0,0));
        this.textQuitter.setPadding(new Insets(22,0,0,0));
        //this.setMaxHeight(5000);
        this.setMaxWidth(200);
    }
}
