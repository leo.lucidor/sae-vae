import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import java.util.List;

public class BibVente {

    private static final int VENTES_PAR_LIGNE = 1;
    private static final int BOX_WIDHT = 1300;
    private static final int BOX_HEIGHT = 100;

    public static HBox createHBoxVentes(List<List<String>> ventes, ApplicationVAE appli) {
        HBox hbox = new HBox();
        hbox.setSpacing(10);
        hbox.setPadding(new Insets(10,0,0,0));

        for (List<String> vente : ventes) {
            VBox vbox = createVBoxElement(vente, appli);
            hbox.getChildren().add(vbox);
        }
   
        return hbox;
    }

    public static VBox createVBoxElement(List<String> vente, ApplicationVAE appli) {
        VBox vbox = new VBox();

        Label label = new Label("Vendeur : " + vente.get(0) + "\n" +
                "Nom objet : " + vente.get(1) + "\n" +
                "Prix base : " + vente.get(2) + "€" + "\n" +
                "Prix mini : " + vente.get(3) + "€" + "\n" +
                "Ville : " + vente.get(5));

        HBox hboxBouton = new HBox();
        Button button = new Button("Voir plus");
        button.setOnAction(new ControleurEncherir(appli, vente));
        hboxBouton.getChildren().add(button);
        hboxBouton.setAlignment(javafx.geometry.Pos.CENTER);
        vbox.getChildren().addAll(label, hboxBouton);
        label.setStyle("-fx-text-fill: #000000;");
        label.setPadding(new Insets(5,0,0,10));
        vbox.setBackground(new Background(
                new BackgroundFill(Color.web("#4A919E"), new CornerRadii(10), null)
        ));
        vbox.setPrefSize(BOX_WIDHT, BOX_HEIGHT);
        return vbox;
    }

    public static VBox createVBoxElements(List<List<String>> elements, ApplicationVAE appli) {
        VBox mainVBox = new VBox();
        mainVBox.setSpacing(10);

        for (int i = 0; i < elements.size(); i += VENTES_PAR_LIGNE) {
            List<List<String>> sublist = elements.subList(i, Math.min(i + VENTES_PAR_LIGNE, elements.size()));
            HBox hbox = createHBoxVentes(sublist, appli);
            mainVBox.getChildren().add(hbox);
        }
        return mainVBox;
    }

    public static VBox EncherePage(List<String> enchere) {
        VBox enchereVBox = new VBox();
        enchereVBox.setSpacing(10);

        Label label = new Label("Enchère : " + enchere.get(1) + "\n" +
                "Vendeur : " + enchere.get(0) + "\n" +
                "Quantité: " + enchere.get(2) + "\n" +
                "Prix: " + enchere.get(3) + "€");

        label.setStyle("-fx-text-fill: #00aef0;");
        enchereVBox.getChildren().add(label);
        return enchereVBox;
    }
}
