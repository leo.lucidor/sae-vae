import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurRetourPageAccueil implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    private Utilisateur utilisateur;
    
    public ControleurRetourPageAccueil(ApplicationVAE appli, Utilisateur utilisateur){
        this.appli = appli;
        this.utilisateur = utilisateur;
    }

    @Override
    public void handle(ActionEvent event) {
        int role = this.utilisateur.getRole();
        if(role == 1){
            this.appli.pageAcceuilAdministrateur();
            this.appli.setGrandPage(true);
        }
        else {
            this.appli.pageAcceuilUtilisateur();
            this.appli.setGrandPage(true);
        }
    }
}