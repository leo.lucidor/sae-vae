import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.event.ActionEvent;

public class ControleurRetourMenuConnexion implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurRetourMenuConnexion(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageConnexion();
    }
}
