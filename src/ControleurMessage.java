import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurMessage implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurMessage(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageMessage();
    }
}