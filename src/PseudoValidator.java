public class PseudoValidator {
    public static boolean isValidPseudo(String pseudo) {
        return pseudo.length() >= 3;
    }
}
