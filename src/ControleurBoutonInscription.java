import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonInscription implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurBoutonInscription(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageInscription();
    }
}
