import javafx.event.EventHandler;
import javafx.scene.layout.VBox;

import java.util.List;
import javafx.event.ActionEvent;

public class ControleurEncherir implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    private List<String> vente;
    
    public ControleurEncherir(ApplicationVAE appli, List<String> vente){
        this.appli = appli;
        this.vente = vente;
    }

    @Override
    public void handle(ActionEvent event) {
        VBox encherePage = BibVente.EncherePage(this.vente);
        this.appli.pageEncherir(encherePage);
    }
}
