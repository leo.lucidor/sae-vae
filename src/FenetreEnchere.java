import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import java.util.List;
import java.sql.SQLException;


public class FenetreEnchere extends StackPane {

    private Vente venteModele;
    private List<List<String>> lesVentes;
    private static final int VENTES_PAR_LIGNE = 1;
    private int choixSelectionneProduit = 0;
    private int choixSelectionneTrie = 1;
    private String rechercheText = "";
    private String villeRayonText = "";
    private int prixMax = 1000000;

    public FenetreEnchere(ApplicationVAE appli, Vente venteModele, BorderPane borderMenuOption, Label textMenu, Label textPageAccueil, Label textVendreUnProduit, Label textVenteAuxEncheres, Label textMessagerie, Label textMonPanier, Label textInformations, Label textSetting, Label textQuitter, Button quitter, Button deconnexion, Button menu,  Button acceuil, Button message, Button info, Button setting, Button profil, Button enchere, Button panier, Button ajouterEnchere){
        this.venteModele = venteModele;
        Slider sliderTriePrix = new Slider();
        // Définir les valeurs minimum, maximum et initial du Slider
        //sliderTriePrix.setMin(0);
        //sliderTriePrix.setMax(100);
        //sliderTriePrix.setValue(50);
        TextField recherche = new TextField();
        TextField villeRayon = new TextField();
        ComboBox<String> categorie = new ComboBox<String>();
        ComboBox<String> triePrix = new ComboBox<String>();
        BorderPane bpPage = new BorderPane();
        HBox hbTopContainer = new HBox();
        VBox vbTopContainer = new VBox();
        HBox hbTop = new HBox();
        HBox hbTop2 = new HBox();
        HBox hbRecherche = new HBox();
        HBox hbVilleRayon = new HBox();
        HBox hbCategorie = new HBox();
        HBox hbTriePrix = new HBox();
        VBox vBoxCenter = new VBox();
        VBox vBoxVente = new VBox();
        CheckBox checkBoxFiltre = new CheckBox("Voir les annonces possibles en livraison");
        Label labelTopTitre = new Label("Vous êtes à la recherche d’un produit en particulier ? Vous êtes au bon endroit !");
        Label labelTriePrixMax = new Label("Prix maximum : "+this.prixMax+"€");
        ScrollPane scrollPane = new ScrollPane(vBoxVente);
        scrollPane.setStyle("-fx-background-color: transparent; -fx-background-insets: 0; -fx-padding: 0;");
        scrollPane.setFitToHeight(true);
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        HBox hBoxLeft = new HBox();
        BorderPane rootLeftOption = borderMenuOption;
        BorderPane rootLeft = new MenuLeft(acceuil, quitter, message, info, setting, enchere, panier, ajouterEnchere, bpPage);
        hBoxLeft.getChildren().addAll(rootLeft, rootLeftOption);
        vBoxCenter.getChildren().addAll(hbTopContainer, scrollPane);
        bpPage.setLeft(hBoxLeft);
        BorderPane rootTop = new MenuTop(textMenu, deconnexion, profil, menu, bpPage);
        bpPage.setTop(rootTop);
        this.getChildren().add(bpPage);
        this.getChildren().add(rootLeftOption);
        //hbTop.setPadding(new Insets(20,50,0,50));
        // Ajoutez les 5 choix au ComboBox
        categorie.getItems().addAll("Aucune catégorie", "Vêtement", "Ustensile Cuisine", "Meuble", "Outil");
        categorie.getSelectionModel().selectFirst();
        triePrix.getItems().addAll("Prix croissant", "Prix décroissant");
        triePrix.getSelectionModel().selectFirst();
        hbRecherche.getChildren().addAll(recherche);
        hbVilleRayon.getChildren().add(villeRayon);
        hbCategorie.getChildren().add(categorie);
        hbTriePrix.getChildren().add(triePrix);
        hbTopContainer.getChildren().addAll(vbTopContainer);
        vbTopContainer.getChildren().addAll(labelTopTitre, hbTop, hbTop2, checkBoxFiltre);
        hbTop.getChildren().addAll(hbCategorie, hbRecherche, hbVilleRayon);
        hbTop2.getChildren().addAll(hbTriePrix, labelTriePrixMax, sliderTriePrix);
        //labelTop.setStyle("-fx-background-color: #BED3C3;");
        //hbTop.setStyle("-fx-background-color: #BED3C3;");
        hbTop.setAlignment(Pos.CENTER);
        labelTopTitre.setPadding(new Insets(10,50,0,100));
        checkBoxFiltre.setPadding(new Insets(0,0,30,15));
        //checkBoxFiltre.setAlignment(Pos.CENTER);
        vbTopContainer.setStyle("-fx-background-color: #BED3C3;");
        vBoxVente.setStyle("-fx-background-color: #012245;");
        vBoxCenter.setStyle("-fx-background-color: #012245;");
        vBoxVente.prefWidthProperty().bind(scrollPane.widthProperty());
        this.setStyle("-fx-background-color: #012245;");
        bpPage.setStyle("-fx-background-color: #012245;");
        bpPage.setCenter(vBoxCenter);
        recherche.setPrefHeight(30);
        recherche.setMinWidth(300);
        villeRayon.setPrefHeight(30);
        villeRayon.setMinWidth(150);
        hbRecherche.setPadding(new Insets(0,0,0,10));
        hbVilleRayon.setPadding(new Insets(0,20,0,10));
        hbCategorie.setPadding(new Insets(0,0,0,20));
        vBoxVente.setPadding(new Insets(20,0,20,250));
        hbTop.setPadding(new Insets(20,0,20,0));
        hbTop2.setPadding(new Insets(0,0,0,20));
        labelTriePrixMax.setPadding(new Insets(0,10,0,20));
        hbTopContainer.setAlignment(Pos.CENTER);
        hbTopContainer.setPadding(new Insets(20,0,0,0));
        recherche.setPromptText("Rechercher un produit");
        villeRayon.setPromptText("Rechercher une ville");
        vBoxVente.setAlignment(Pos.CENTER);
        this.setMargin(rootLeftOption, new Insets(48,0,0,56));
        this.setAlignment(rootLeftOption, Pos.TOP_LEFT);
        try {
            this.lesVentes = this.venteModele.getVente("", "");
            this.prixMax = this.venteModele.getMaxPrixVente(this.lesVentes);
            if(this.prixMax != 0){
                sliderTriePrix.setMax(this.prixMax);
            } 
            sliderTriePrix.setMin(0);
            sliderTriePrix.setValue(this.prixMax);
            labelTriePrixMax.setText("Prix maximum : " + this.prixMax + "€");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        vBoxVente.getChildren().clear();
        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
            vBoxVente.getChildren().add(hbox);
        }
        // Ajout d'un écouteur d'événements pour mettre à jour la variable int lorsque la valeur du Slider change
        sliderTriePrix.valueProperty().addListener((observable, oldValue, newValue) -> {
            this.prixMax = newValue.intValue();
            labelTriePrixMax.setText("Prix maximum : " + this.prixMax + "€");
            try {
                if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie == 0){
                    this.lesVentes = this.venteModele.getVente(this.rechercheText, this.villeRayonText);
                }    
                else {
                    if(this.choixSelectionneProduit != 0 && this.choixSelectionneTrie == 0){
                        this.lesVentes = this.venteModele.getVenteAvecCategorie(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit);
                    }
                    else if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie != 0){
                        if(this.choixSelectionneTrie == 1){
                            this.lesVentes = this.venteModele.getVenteAvecPrixCroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        }
                        else if(this.choixSelectionneTrie == 2){
                            this.lesVentes = this.venteModele.getVenteAvecPrixDecroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        }
                    }
                    else{
                        if(this.choixSelectionneTrie == 1){
                            this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixCroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        }
                        else if(this.choixSelectionneTrie == 2){
                            this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixDecroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        }
                    }
                }
                vBoxVente.getChildren().clear();
                for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                    List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                    HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                    vBoxVente.getChildren().add(hbox);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        recherche.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                this.rechercheText = newValue;
                if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie == 0){
                    this.lesVentes = this.venteModele.getVente(this.rechercheText, this.villeRayonText);
                }    
                else {
                    if(this.choixSelectionneProduit != 0 && this.choixSelectionneTrie == 0){
                        this.lesVentes = this.venteModele.getVenteAvecCategorie(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit);
                    }
                    else if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie != 0){
                        if(this.choixSelectionneTrie == 1){
                            this.lesVentes = this.venteModele.getVenteAvecPrixCroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        }
                        else if(this.choixSelectionneTrie == 2){
                            this.lesVentes = this.venteModele.getVenteAvecPrixDecroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        }
                    }
                    else{
                        if(this.choixSelectionneTrie == 1){
                            this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixCroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        }
                        else if(this.choixSelectionneTrie == 2){
                            this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixDecroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        }
                    }
                }
                this.prixMax = this.venteModele.getMaxPrixVente(this.lesVentes);
                if(this.prixMax != 0){
                sliderTriePrix.setMax(this.prixMax);
                } 
                //sliderTriePrix.setValue(this.prixMax);
                labelTriePrixMax.setText("Prix maximum : " + this.prixMax + "€");
                vBoxVente.getChildren().clear();
                for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                    List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                    HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                    vBoxVente.getChildren().add(hbox);
                }
                //System.out.println(this.venteModele.getVenteAvecCategorie(newValue, 4));
                //System.out.println(this.venteModele.getVente(newValue));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        villeRayon.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                this.villeRayonText = newValue;
                if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie == 0){
                    this.lesVentes = this.venteModele.getVente(this.rechercheText, this.villeRayonText);
                }    
                else {
                    if(this.choixSelectionneProduit != 0 && this.choixSelectionneTrie == 0){
                        this.lesVentes = this.venteModele.getVenteAvecCategorie(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit);
                    }
                    else if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie != 0){
                        if(this.choixSelectionneTrie == 1){
                            this.lesVentes = this.venteModele.getVenteAvecPrixCroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        }
                        else if(this.choixSelectionneTrie == 2){
                            this.lesVentes = this.venteModele.getVenteAvecPrixDecroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        }
                    }
                    else{
                        if(this.choixSelectionneTrie == 1){
                            this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixCroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        }
                        else if(this.choixSelectionneTrie == 2){
                            this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixDecroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        }
                    }
                }
                this.prixMax = this.venteModele.getMaxPrixVente(this.lesVentes);
                if(this.prixMax != 0){
                sliderTriePrix.setMax(this.prixMax);
                } 
               //sliderTriePrix.setValue(this.prixMax);
                labelTriePrixMax.setText("Prix maximum : " + this.prixMax + "€");
                vBoxVente.getChildren().clear();
                for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                    List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                    HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                    vBoxVente.getChildren().add(hbox);
                }
                //System.out.println(this.venteModele.getVenteAvecCategorie(newValue, 4));
                //System.out.println(this.venteModele.getVente(newValue));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        // Ajoutez un ChangeListener au ComboBox
        categorie.valueProperty().addListener((observable, oldValue, newValue) -> {
            // Mettez à jour la variable choixSelectionneProduit avec le nouveau choix
            if(newValue == "Aucune catégorie"){
                this.choixSelectionneProduit = 0;
            }
            else if(newValue == "Vêtement"){
                this.choixSelectionneProduit = 1;
            }
            else if(newValue == "Ustensile Cuisine"){
                this.choixSelectionneProduit = 2;
            }
            else if(newValue == "Meuble"){
                this.choixSelectionneProduit = 3;
            }
            else if(newValue == "Outil"){
                this.choixSelectionneProduit = 4;
            }
            vBoxVente.getChildren().clear();
            try {
                if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie == 0){
                    this.lesVentes = this.venteModele.getVente(this.rechercheText, this.villeRayonText);
                    for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                        List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                        HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                        vBoxVente.getChildren().add(hbox);
                    }
                }
                else if(this.choixSelectionneProduit != 0 && this.choixSelectionneTrie == 0){
                    this.lesVentes = this.venteModele.getVenteAvecCategorie(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit);
                    for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                        List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                        HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                        vBoxVente.getChildren().add(hbox);
                    }
                }
                else if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie != 0){
                    if(this.choixSelectionneTrie == 1){
                        this.lesVentes = this.venteModele.getVenteAvecPrixCroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                            vBoxVente.getChildren().add(hbox);
                        } 
                    }
                    else if(this.choixSelectionneTrie == 2){
                        this.lesVentes = this.venteModele.getVenteAvecPrixDecroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                            vBoxVente.getChildren().add(hbox);
                        } 
                    }
                }
                else {
                    if(this.choixSelectionneTrie == 1){
                        this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixCroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                            vBoxVente.getChildren().add(hbox);
                        } 
                    }
                    else if(this.choixSelectionneTrie == 2){
                        this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixDecroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                            vBoxVente.getChildren().add(hbox);
                        } 
                    }
                }
                this.prixMax = this.venteModele.getMaxPrixVente(this.lesVentes);
                if(this.prixMax != 0){
                    sliderTriePrix.setMax(this.prixMax);
                } 
                labelTriePrixMax.setText("Prix maximum : " + this.prixMax + "€");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        });
        triePrix.valueProperty().addListener((observable, oldValue, newValue) -> {
            // Mettez à jour la variable choixSelectionneProduit avec le nouveau choix
            if(newValue == "Aucun trie"){
                this.choixSelectionneTrie = 0;
            }
            else if(newValue == "Prix croissant"){
                this.choixSelectionneTrie = 1;
            }
            else if(newValue == "Prix d\u00E9croissant"){
                this.choixSelectionneTrie = 2;
            }
            vBoxVente.getChildren().clear();
            try {
                if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie == 0){
                    this.lesVentes = this.venteModele.getVente(this.rechercheText, this.villeRayonText);
                    for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                        List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                        HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                        vBoxVente.getChildren().add(hbox);
                    }
                }
                else if(this.choixSelectionneProduit != 0 && this.choixSelectionneTrie == 0){
                    this.lesVentes = this.venteModele.getVenteAvecCategorie(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit);
                    for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                        List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                        HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                        vBoxVente.getChildren().add(hbox);
                    }
                }
                else if(this.choixSelectionneProduit == 0 && this.choixSelectionneTrie != 0){
                    if(this.choixSelectionneTrie == 1){
                        this.lesVentes = this.venteModele.getVenteAvecPrixCroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                            vBoxVente.getChildren().add(hbox);
                        } 
                    }
                    else if(this.choixSelectionneTrie == 2){
                        this.lesVentes = this.venteModele.getVenteAvecPrixDecroissant(this.rechercheText, this.villeRayonText, this.prixMax);
                        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                            vBoxVente.getChildren().add(hbox);
                        } 
                    }
                }
                else {
                    if(this.choixSelectionneTrie == 1){
                        this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixCroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                            vBoxVente.getChildren().add(hbox);
                        } 
                    }
                    else if(this.choixSelectionneTrie == 2){
                        this.lesVentes = this.venteModele.getVenteAvecCategorieEtPrixDecroissant(this.rechercheText, this.villeRayonText, this.choixSelectionneProduit, this.prixMax);
                        for (int i = 0; i < this.lesVentes.size(); i += VENTES_PAR_LIGNE) {
                            List<List<String>> sublist = this.lesVentes.subList(i, Math.min(i + VENTES_PAR_LIGNE, this.lesVentes.size()));
                            HBox hbox = BibVente.createHBoxVentes(sublist, appli);
                            vBoxVente.getChildren().add(hbox);
                        } 
                    }
                }
                this.prixMax = this.venteModele.getMaxPrixVente(this.lesVentes);
                if(this.prixMax != 0){
                    sliderTriePrix.setMax(this.prixMax);
                } 
                //sliderTriePrix.setValue(this.prixMax);
                labelTriePrixMax.setText("Prix maximum : " + this.prixMax + "€");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        });
    }    
}