import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;

public class AffichageElements extends Application {

    private static final int VENTES_PAR_LIGNE = 5;

    @Override
    public void start(Stage primaryStage) {
        List<List<String>> elements = Arrays.asList(
                Arrays.asList("toisopu452", "Pantalon de qualité", "4", "29"),
                Arrays.asList("aokl984", "Chemise élégante", "2", "49"),
                Arrays.asList("fgeopd625", "Chaussures de sport", "6", "79"),
                Arrays.asList("qwerty123", "Robe d'été", "3", "39"),
                Arrays.asList("abc456", "Veste en cuir", "1", "99"),
                Arrays.asList("xyz789", "Sac à dos", "5", "59"),
                Arrays.asList("plm741", "T-shirt rayé", "8", "19"),
                Arrays.asList("qazwsx852", "Jupe plissée", "2", "35"),
                Arrays.asList("edcrfv963", "Chapeau de paille", "7", "15"),
                Arrays.asList("poi098", "Short en jean", "3", "25")
        );

        VBox mainVBox = createVBoxElements(elements);

        Scene scene = new Scene(mainVBox, 500, 400);

        primaryStage.setTitle("Affichage des ventes");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private VBox createVBoxElements(List<List<String>> elements) {
        VBox mainVBox = new VBox();
        mainVBox.setSpacing(10);

        for (int i = 0; i < elements.size(); i += VENTES_PAR_LIGNE) {
            List<List<String>> sublist = elements.subList(i, Math.min(i + VENTES_PAR_LIGNE, elements.size()));
            HBox hbox = createHBoxVentes(sublist);
            mainVBox.getChildren().add(hbox);
        }

        return mainVBox;
    }

    private HBox createHBoxVentes(List<List<String>> ventes) {
        HBox hbox = new HBox();
        hbox.setSpacing(10);

        for (List<String> vente : ventes) {
            VBox vbox = createVBoxElement(vente);
            hbox.getChildren().add(vbox);
        }

        return hbox;
    }

    private VBox createVBoxElement(List<String> vente) {
        VBox vbox = new VBox();

        Label label = new Label("Vendeur : " + vente.get(0) + "\n" +
                "Description: " + vente.get(1) + "\n" +
                "Quantité: " + vente.get(2) + "\n" +
                "Prix: " + vente.get(3) + "€");

        Button button = new Button("Effectuer une enchère");
        button.setOnAction(event -> openEncherePage(vente));

        vbox.getChildren().addAll(label, button);
        vbox.setStyle("-fx-border-color: black; -fx-padding: 10px;");
        vbox.setPrefWidth(250);
        vbox.setPrefHeight(150);

        return vbox;
    }

    private void openEncherePage(List<String> enchere) {
        Stage enchereStage = new Stage();
        VBox enchereVBox = new VBox();
        enchereVBox.setSpacing(10);

        Label label = new Label("Enchère : " + enchere.get(1) + "\n" +
                "Vendeur : " + enchere.get(0) + "\n" +
                "Quantité: " + enchere.get(2) + "\n" +
                "Prix: " + enchere.get(3) + "€");

        enchereVBox.getChildren().add(label);

        Scene enchereScene = new Scene(enchereVBox, 300, 200);

        enchereStage.setTitle("Enchère");
        enchereStage.setScene(enchereScene);
        enchereStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
