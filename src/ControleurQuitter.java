import javafx.event.EventHandler;
import javafx.application.Platform;
import javafx.event.ActionEvent;

public class ControleurQuitter implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurQuitter(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.popUpQuitterApplication();
    }
}