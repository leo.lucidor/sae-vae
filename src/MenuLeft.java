import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;

public class MenuLeft extends BorderPane {

    private Button boutonAccueil;
    private Button boutonMessage;
    private Button boutonInfo;
    private Button boutonSetting;
    private Button boutonEnchere;
    private Button boutonPanier;
    private Button boutonAjouterVente;
    private Button boutonQuitter;

    public MenuLeft(Button accueil, Button quitter, Button message, Button info, Button setting, Button enchere, Button panier, Button ajouterEnchere, BorderPane root){
        this.boutonAccueil = accueil;
        this.boutonMessage = message;
        this.boutonInfo = info;
        this.boutonSetting = setting;
        this.boutonEnchere = enchere;
        this.boutonPanier = panier;
        this.boutonAjouterVente = ajouterEnchere;
        this.boutonQuitter = quitter;
        this.setBackground(new Background(new BackgroundFill(Color.web("#1F1F1F"), new CornerRadii(0), Insets.EMPTY)));
        VBox vBoxTop = new VBox();
        VBox vBoxBottom = new VBox();
        ImageView accueilIMG = new ImageView(new Image("file:./images/logoAppli.png"));
        accueilIMG.setFitHeight(40);
        accueilIMG.setFitWidth(40);
        boutonAccueil.setGraphic(accueilIMG);
        boutonAccueil.setStyle("-fx-background-color: #1F1F1F");
        ImageView infoIMG = new ImageView(new Image("file:./images/iconInfo.png"));
        infoIMG.setFitHeight(40);
        infoIMG.setFitWidth(40);
        boutonInfo.setGraphic(infoIMG);
        boutonInfo.setStyle("-fx-background-color: #1F1F1F");
        ImageView settingIMG = new ImageView(new Image("file:./images/iconSetting.png"));
        settingIMG.setFitHeight(40);
        settingIMG.setFitWidth(40);
        boutonSetting.setGraphic(settingIMG);
        boutonSetting.setStyle("-fx-background-color: #1F1F1F");
        ImageView messageIMG = new ImageView(new Image("file:./images/iconMessage.png"));
        messageIMG.setFitHeight(40);
        messageIMG.setFitWidth(40);
        boutonMessage.setGraphic(messageIMG);
        boutonMessage.setStyle("-fx-background-color: #1F1F1F");
        ImageView enchereIMG = new ImageView(new Image("file:./images/iconEnchere.png"));
        enchereIMG.setFitHeight(40);
        enchereIMG.setFitWidth(40);
        boutonEnchere.setGraphic(enchereIMG);
        boutonEnchere.setStyle("-fx-background-color: #1F1F1F");
        ImageView panierIMG = new ImageView(new Image("file:./images/iconPanier.png"));
        panierIMG.setFitHeight(40);
        panierIMG.setFitWidth(40);
        boutonPanier.setGraphic(panierIMG);
        boutonPanier.setStyle("-fx-background-color: #1F1F1F");
        ImageView ajouterVenteIMG = new ImageView(new Image("file:./images/iconAjouterVente.png"));
        ajouterVenteIMG.setFitHeight(40);
        ajouterVenteIMG.setFitWidth(40);
        boutonAjouterVente.setGraphic(ajouterVenteIMG);
        boutonAjouterVente.setStyle("-fx-background-color: #1F1F1F");
        ImageView quitterIMG = new ImageView(new Image("file:./images/iconQuitter.png"));
        quitterIMG.setFitHeight(40);
        quitterIMG.setFitWidth(40);
        boutonQuitter.setGraphic(quitterIMG);
        boutonQuitter.setStyle("-fx-background-color: #1F1F1F");
        vBoxTop.getChildren().addAll(boutonAccueil, boutonMessage, boutonEnchere, boutonPanier, boutonAjouterVente);
        vBoxBottom.getChildren().addAll(boutonInfo, boutonSetting, boutonQuitter);
        // mettre les toolTip
        Tooltip tooltipAccueil = new Tooltip();
        tooltipAccueil.setText("Page d'accueil");
        tooltipAccueil.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonAccueil.setTooltip(tooltipAccueil);
        Tooltip tooltipMessage = new Tooltip();
        tooltipMessage.setText("Page message");
        tooltipMessage.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonMessage.setTooltip(tooltipMessage);
        Tooltip tooltipInfo = new Tooltip();
        tooltipInfo.setText("Page information");
        tooltipInfo.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonInfo.setTooltip(tooltipInfo);
        Tooltip tooltipSetting = new Tooltip();
        tooltipSetting.setText("Page paramètre");
        tooltipSetting.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonSetting.setTooltip(tooltipSetting);
        Tooltip tooltipEnchere = new Tooltip();
        tooltipEnchere.setText("Page enchère");
        tooltipEnchere.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonEnchere.setTooltip(tooltipEnchere);
        Tooltip tooltipPanier = new Tooltip();
        tooltipPanier.setText("Page panier");
        tooltipPanier.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonPanier.setTooltip(tooltipPanier);
        Tooltip tooltipAjouterVente = new Tooltip();
        tooltipAjouterVente.setText("Page ajouter une vente");
        tooltipAjouterVente.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonAjouterVente.setTooltip(tooltipAjouterVente);
        Tooltip tooltipQuitter = new Tooltip();
        tooltipQuitter.setText("Quitter l'application");
        tooltipQuitter.setShowDelay(Duration.millis(400)); // Définir la durée d'affichage du Tooltip (en millisecondes)
        this.boutonQuitter.setTooltip(tooltipQuitter);
        this.setTop(vBoxTop);
        this.setBottom(vBoxBottom);
        root.setLeft(this);
    }
    
}
