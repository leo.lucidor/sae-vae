import javafx.scene.control.Button;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.image.Image;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;


public class FenetreAccueil extends BorderPane {
    
    private VBox vBoxConnexion;
    private HBox hBoxBouton;
    private HBox hBoxMdpOublie;
    private Button boutonConnexion;
    private Button boutonInscription;
    private Button mdpOublie;
    private TextField identifiant;
    private PasswordField motDePasse;
    private Label videConnexion;
    private Label videBouton;
    private Label mauvaiseConnexion;
    

    public FenetreAccueil(Button mdpOublie, Button inscription, Button connexion, TextField identifiant, PasswordField mdp, Label mauvaiseConnexion){
        vBoxConnexion = new VBox();
        hBoxBouton = new HBox();
        hBoxMdpOublie = new HBox();
        // création des boutons
        this.boutonConnexion = connexion;
        this.boutonInscription = inscription;
        this.boutonConnexion.getStyleClass().add("button");
        // styles des boutons
        boutonInscription.setStyle("-fx-background-color: #00aef0; -fx-text-fill: black;");
        boutonConnexion.setStyle("-fx-background-color: #00aef0; -fx-text-fill: black;");
        // création des textField
        this.identifiant = identifiant;
        this.motDePasse = mdp;
        this.identifiant.setPromptText("Enter your username");
        this.motDePasse.setPromptText("Enter your password");
        this.identifiant.getStyleClass().add("text");
        this.motDePasse.getStyleClass().add("text");
        // style textfield
        this.identifiant.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        this.motDePasse.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        // création des labels vide
        this.videConnexion = new Label("    ");
        this.videBouton = new Label("    ");
        this.mauvaiseConnexion = mauvaiseConnexion;
        this.mdpOublie = mdpOublie;
        this.mauvaiseConnexion.setStyle("-fx-text-fill: red;");
        this.mauvaiseConnexion.setPadding(new Insets(10, 0, 0, 80));
        this.mauvaiseConnexion.setVisible(false);
        this.mdpOublie.setStyle("-fx-text-fill: #00aef0; -fx-background-color: #012245;");
        //this.mdpOublie.setVisible(false);
        // ajout des textField
        vBoxConnexion.getChildren().addAll(this.identifiant, this.videConnexion, this.motDePasse, this.mauvaiseConnexion, this.hBoxMdpOublie);
        // ajout des boutons
        hBoxBouton.getChildren().addAll(this.boutonInscription, this.videBouton, this.boutonConnexion);
        hBoxMdpOublie.getChildren().add(this.mdpOublie);
        // placement
        vBoxConnexion.setPadding(new Insets(250, 50, 0, 50));
        hBoxBouton.setPadding(new Insets(30, 0, 100, 160));
        this.hBoxMdpOublie.setPadding(new Insets(5, 0, 0, 120));
        // background
        Image backgroundConnexion = new Image("file:./images/backgroundConnexion.png");
        BackgroundImage backgroundConnexionIMG = new BackgroundImage(backgroundConnexion, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background backgroundConnexionFinal = new Background(backgroundConnexionIMG);
        this.setBackground(backgroundConnexionFinal);
        // placement
        this.setCenter(vBoxConnexion);
        this.setBottom(hBoxBouton);
    }
}
