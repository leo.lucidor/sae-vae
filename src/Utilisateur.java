import java.sql.SQLException;
import java.util.List;

public class Utilisateur {
    private BaseDeDonnee baseDeDonnee;
    private int role;
    private String pseudo;
    private String mail;
    private String mdp;
    private String genre;
    private String nom;
    private String prenom;
    private int age;
    private int like;
    
    public Utilisateur() throws SQLException{
        this.baseDeDonnee = new BaseDeDonnee("./bd/VAE.db");
    }

    public int getLike(){
        return this.like;
    }

    public int getRole(){
        return this.role;
    }

    public String getPseudo(){
        return this.pseudo;
    }

    public String getMail(){
        return this.mail;
    }

    public String getMdp(){
        return this.mdp;
    }

    public String getGenre(){
        return this.genre;
    }

    public String getNom(){
        return this.nom;
    }

    public String getPrenom(){
        return this.prenom;
    }

    public int getAge(){
        return this.age;
    }

    public void setRole(int role){
        this.role = role;
    }

    public void setPseudo(String pseudo){
        this.pseudo = pseudo;
    }

    public void setMail(String mail){
        this.mail = mail;
    }

    public void setMdp(String mdp){
        this.mdp = mdp;
    }

    public void setGenre(String genre){
        this.genre = genre;
    }

    public void setNom(String nom){
        this.nom = nom;
    }

    public void setPrenom(String prenom){
        this.prenom = prenom;
    }

    public void setAge(int age){
        this.age = age;
    }

    public void incrementerLike(){
        this.like++;
    }

    public void decrementerLike(){
        this.like--;
    }

    public String getMdp (String pseudo) throws SQLException{
        try{
            return this.baseDeDonnee.getMdpPseudo(pseudo);
        } catch (SQLException e){
            return this.baseDeDonnee.getMdpMail(pseudo);
        }
    }

    public String getMdpMail (String mail) throws SQLException{
        return this.baseDeDonnee.getMdpMail(mail);
    }

    public int getId(String pseudo) throws SQLException{
        return this.baseDeDonnee.getId(pseudo);
    }

    public String getRole(int id) throws SQLException{
        return this.baseDeDonnee.getRole(id);
    }

    public String getMdp(int id) throws SQLException{
        return this.baseDeDonnee.getMdp(id);
    }

    public List<String> getVenteUtilisateur(String nom) throws SQLException{
        return this.baseDeDonnee.getVenteUtilisateur(nom);
    }

    public void insertUtilisateur(String pseudo, String mail, String mdp, int role) throws SQLException{
        this.baseDeDonnee.insertUtilisateur(pseudo, mail, mdp, "N", role);
    }
}