import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurEnchere implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurEnchere(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageEnchere();
    }
}