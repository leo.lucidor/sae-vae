import javafx.scene.control.Button;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Background;
import javafx.scene.image.Image;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ComboBox;
import javafx.scene.paint.Color;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.image.ImageView;

public class FenetreMotDePasseOublie extends BorderPane {

    private HBox hBoxTop;
    private HBox hBoxBottom;
    private VBox vBoxCenter;
    private Button boutonRetour;
    private Button valider;
    private TextField mail;
    private Label titre;
    private Label mauvaisMail;

    public FenetreMotDePasseOublie(Button retourMenu, TextField mail, Button valider, Label mauvaisMail){
        this.setBackground(new Background(new BackgroundFill(Color.web("#012245"), new CornerRadii(0), Insets.EMPTY)));
        this.titre = new Label("Mot de passe oublié");
        this.hBoxTop = new HBox();
        this.hBoxBottom = new HBox();
        this.vBoxCenter = new VBox();
        this.mail = mail;
        this.mail.clear();
        this.mauvaisMail = mauvaisMail;
        this.boutonRetour = retourMenu;
        this.valider = valider;
        this.mauvaisMail.setStyle("-fx-text-fill: red;");
        this.mauvaisMail.setVisible(false);
        this.mail.setPromptText("Entrée votre mail");
        this.mail.setStyle( "-fx-background-color: #012245;" + "-fx-border-color: #00aef0;" + "-fx-border-width: 0 0 1 0;" +  "-fx-font-size: 14px;" + "-fx-text-fill: #00aef0; -fx-prompt-text-fill: #00aef0;");
        ImageView retourMenuIMG = new ImageView(new Image("file:./images/iconRetourMenu.png"));
        retourMenuIMG.setFitHeight(40);
        retourMenuIMG.setFitWidth(40);
        this.boutonRetour.setGraphic(retourMenuIMG);
        this.boutonRetour.setStyle("-fx-background-color: #012345");
        this.valider.setStyle("-fx-background-color: #00aef0; -fx-text-fill: black;");
        titre.setStyle("-fx-text-fill: #00aef0; -fx-font-size: 20px;");
        titre.setPadding(new Insets(10, 0, 0, 110));
        this.mauvaisMail.setPadding(new Insets(20, 0, 0, 90));
        this.hBoxTop.setPadding(new Insets(10, 0, 0, 0));
        this.vBoxCenter.setPadding(new Insets(180, 100, 0, 100));
        this.hBoxBottom.setPadding(new Insets(30, 0, 0, 68));
        this.hBoxBottom.getChildren().add(this.valider);
        this.hBoxTop.getChildren().addAll(boutonRetour, titre);
        this.vBoxCenter.getChildren().addAll(this.mail, this.mauvaisMail, this.hBoxBottom);
        this.setTop(this.hBoxTop);
        this.setCenter(this.vBoxCenter);
        //this.setBottom(this.hBoxBottom);
    }
}
