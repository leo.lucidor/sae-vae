import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurPageMotDePasseOublie  implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurPageMotDePasseOublie(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageMotDePasseOublie();
    }
}
