import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.geometry.Insets;
import javafx.scene.paint.Color;

public class ControleurMenu implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    private BorderPane borderMenuOption;
    
    public ControleurMenu(ApplicationVAE appli, BorderPane borderMenuOption){
        this.appli = appli;
        this.borderMenuOption = borderMenuOption;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.changerVisibiliteMenu();
        if(this.appli.getMenuCacher()){
            this.borderMenuOption.setBackground(new Background(new BackgroundFill(Color.web("#1F1F1F"), new CornerRadii(0), Insets.EMPTY)));
        }
    }
}