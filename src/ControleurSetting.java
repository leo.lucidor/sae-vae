import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurSetting implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurSetting(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageSetting();
    }
}