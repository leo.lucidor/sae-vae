import java.sql.SQLException;
import java.util.List;

public class Vente {
    private BaseDeDonnee bd;
    
    public Vente() throws SQLException{
        this.bd = new BaseDeDonnee("./bd/VAE.db");
    }

    public List<List<String>> getVente(String nomOb, String nomVille) throws SQLException{
        return this.bd.getVente(nomOb, nomVille);
    }

    public List<List<String>> getVenteAvecCategorie(String nomOb, String nomVille, int categorie) throws SQLException{
        return this.bd.getVenteAvecCategorie(nomOb, nomVille, categorie);
    }

    public List<List<String>> getVenteAvecPrixCroissant(String nomOb, String nomVille, int prixMax) throws SQLException{
        return this.bd.getVenteAvecPrixCroissant(nomOb, nomVille, prixMax);
    }

    public List<List<String>> getVenteAvecPrixDecroissant(String nomOb, String nomVille, int prixMax) throws SQLException{
        return this.bd.getVenteAvecPrixDecroissant(nomOb, nomVille, prixMax);
    }

    public List<List<String>> getVenteAvecCategorieEtPrixCroissant(String nomOb, String nomVille, int categorie, int prixMax) throws SQLException{
        return this.bd.getVenteAvecCategorieEtPrixCroissant(nomOb, nomVille, categorie, prixMax);
    }

    public List<List<String>> getVenteAvecCategorieEtPrixDecroissant(String nomOb, String nomVille, int categorie, int prixMax) throws SQLException{
        return this.bd.getVenteAvecCategorieEtPrixDecroissant(nomOb, nomVille, categorie, prixMax);
    }

    public int getMaxPrixVente(List<List<String>> listeVente) throws SQLException{
        int max = 0;
        for (List<String> vente : listeVente) {
            if (Integer.parseInt(vente.get(3)) > max) {
                max = Integer.parseInt(vente.get(3));
            }
        }
        return max;
    }
}