import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import java.sql.SQLException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;

public class ApplicationVAE extends Application {

    private Button boutonMenu;
    private boolean menuCacher;
    private Button mdpOublie;
    private Button boutonAccueil;
    private Button boutonQuitter;
    private Button boutonInscription;
    private Button boutonFinalisationInscription;
    private Button boutonConnexion;
    private Button boutonDeconnexion;
    private Button boutonRetourMenuConnexion;
    private Button boutonMessage;
    private Button boutonInfo;
    private Button boutonSetting;
    private Button boutonProfil;
    private Button boutonEnchere;
    private Button boutonPanier;
    private Button boutonAjouterVente;
    private Button boutonRecuperationMDP;
    private Label textMenu;
    private Label textPageAccueil;
    private Label textVendreUnProduit;
    private Label textVenteAuxEncheres;
    private Label textMessagerie;
    private Label textMonPanier;
    private Label textInformations;
    private Label textSetting;
    private Label textQuitter;
    private Scene scene;
    private BorderPane root;
    private Stage stage;
    private boolean grandePage;
    private BorderPane borderMenuOption;
    private Utilisateur userModele;
    private Vente venteModele;
    private TextField identifiant;
    private PasswordField mdp;
    private TextField pseudoInscription;
    private TextField mailInscription;
    private TextField mailRecuperationMDP;
    private PasswordField mdpAdministrateurInscription;
    private PasswordField mdpInscription;
    private ComboBox<String> roleInscription;
    private Label mauvaiseConnexion;
    private Label mauvaisPseudo;
    private Label mauvaisMail;
    private Label mauvaisMdp;
    private Label mauvaisMailRecuperationMDP;

    /**
     * Permet de pouvoir executer 
     * @param args les arguments à executer
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() throws SQLException{
        this.mauvaisMailRecuperationMDP = new Label("Le mail n'est pas valide");
        this.mauvaiseConnexion = new Label("Le pseudo ou le mot de passe est incorrect");
        this.mauvaisPseudo = new Label("Le pseudo ne contient pas 3 caractères");
        this.mauvaisMail = new Label("Le mail n'est pas valide");
        this.mauvaisMdp = new Label("       Le mot de passe n'est pas valide \n (8 caractères, 1 chiffre, 1 majuscule, 1 caractère spécial)");
        this.identifiant = new TextField();
        this.pseudoInscription = new TextField();
        this.mailInscription = new TextField();
        this.mdpInscription = new PasswordField();
        this.mailRecuperationMDP = new TextField();
        this.mdpAdministrateurInscription = new PasswordField();
        this.roleInscription = new ComboBox<>();
        String admin = new String("Administrateur");
        String user = new String("Utilisateur");
        this.roleInscription.getItems().addAll(admin, user);
        this.mdp = new PasswordField();
        this.userModele = new Utilisateur();
        this.venteModele = new Vente();
        this.boutonMenu = new Button("");
        this.menuCacher = false;
        this.boutonRecuperationMDP = new Button("Récupérer mon mot de passe");
        this.mdpOublie = new Button("Mot de passe oublié ?");
        this.boutonAccueil = new Button("");
        this.boutonQuitter = new Button("");
        this.boutonConnexion = new Button("Connexion");
        this.boutonInscription = new Button("Inscription");
        this.boutonFinalisationInscription = new Button("Inscription");
        this.boutonDeconnexion = new Button("Déconnexion");
        this.boutonRetourMenuConnexion = new Button("");
        this.boutonMessage = new Button("");
        this.boutonInfo = new Button("");
        this.boutonSetting = new Button("");
        this.boutonProfil = new Button("");
        this.boutonEnchere = new Button("");
        this.boutonPanier = new Button("");
        this.boutonAjouterVente = new Button("");
        this.textMenu = new Label("Menu");
        this.textPageAccueil = new Label("Page d'accueil");
        this.textVendreUnProduit = new Label("Vendre un produit");
        this.textVenteAuxEncheres = new Label("Vente aux enchères");
        this.textMessagerie = new Label("Messagerie");
        this.textMonPanier = new Label("Mon panier");
        this.textInformations = new Label("Informations");
        this.textSetting = new Label("Paramètres");
        this.textQuitter = new Label("Quitter");
        this.borderMenuOption = new MenuOptions(this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter);
        this.boutonRecuperationMDP.setOnAction(new ControleurMotDePasseOublier(this, this.userModele));
        this.mdpOublie.setOnAction(new ControleurPageMotDePasseOublie(this));
        this.boutonAccueil.setOnAction(new ControleurRetourPageAccueil(this, this.userModele));
        this.boutonMenu.setOnAction(new ControleurMenu(this, this.borderMenuOption));
        this.boutonQuitter.setOnAction(new ControleurQuitter(this));
        this.boutonInscription.setOnAction(new ControleurBoutonInscription(this));
        this.boutonFinalisationInscription.setOnAction(new ControleurInscription(this, this.mdpAdministrateurInscription, this.userModele, this.roleInscription));
        this.boutonConnexion.setOnAction(new ControleurChoixTypeUserAdmin(this, this.userModele));
        this.boutonRetourMenuConnexion.setOnAction(new ControleurRetourMenuConnexion(this));
        this.boutonDeconnexion.setOnAction(new ControleurDeconnexion(this));
        this.boutonMessage.setOnAction(new ControleurMessage(this));
        this.boutonInfo.setOnAction(new ControleurInfo(this));
        this.boutonSetting.setOnAction(new ControleurSetting(this));
        this.boutonProfil.setOnAction(new ControleurProfil(this));
        this.boutonEnchere.setOnAction(new ControleurEnchere(this));
        this.boutonPanier.setOnAction(new ControleurPanier(this));
        this.boutonAjouterVente.setOnAction(new ControleurAjouterVente(this));
        this.textMenu.setVisible(false);
        this.borderMenuOption.setVisible(false);
        root = new FenetreAccueil(this.mdpOublie, this.boutonInscription, this.boutonConnexion, this.identifiant, this.mdp, this.mauvaiseConnexion);
        scene = new Scene(root, 500, 500);
        this.grandePage = false;
        this.roleInscription.valueProperty().addListener((observable, oldValue, newValue) -> {
            // Code à exécuter lorsque la sélection de la ComboBox change
            if(newValue.equals("Administrateur")){
                this.changerVisibiliteMdpAdminInscription("Admin");
            }
            else {
                this.changerVisibiliteMdpAdminInscription("User");
            }
        });
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        this.stage.setTitle("Page connexion");
        this.stage.setScene(scene);
        this.stage.setResizable(false);
        this.stage.show();
        // permet de changer l'icone de la fenêtre
        Image iconAppli = new Image("file:./images/logoAppli.png");
        stage.getIcons().add(iconAppli);
    }

    public boolean getMenuCacher(){
        return this.menuCacher;
    }

    public void changerVisibiliteMauvaisMailRecupMDP(){
        this.mauvaisMailRecuperationMDP.setVisible(true);
    }

    public void changerVisibiliteConnexion(){
        this.mauvaiseConnexion.setVisible(true);
    }

    public void changerVisibilitePseudo(){
        this.mauvaisMail.setVisible(false);
        this.mauvaisMdp.setVisible(false);
        this.mauvaisPseudo.setVisible(true);
    }

    public void changerVisibiliteMail(){
        this.mauvaisPseudo.setVisible(false);
        this.mauvaisMdp.setVisible(false);
        this.mauvaisMail.setVisible(true);
    }

    public void changerVisibiliteMdp(){
        this.mauvaisPseudo.setVisible(false);
        this.mauvaisMail.setVisible(false);
        this.mauvaisMdp.setVisible(true);
    }
   
    public void changerVisibiliteMenu(){
        if(this.menuCacher){
            this.borderMenuOption.setVisible(true);
            this.textMenu.setVisible(true);
            this.menuCacher = false;
        }
        else{
            this.borderMenuOption.setVisible(false);
            this.textMenu.setVisible(false);
            this.menuCacher = true;
        }
    }

    public void changerVisibiliteMdpAdminInscription(String type){
        if(type.equals("Admin")){
            this.mdpAdministrateurInscription.setVisible(true);
        }
        else {
            this.mdpAdministrateurInscription.setVisible(false);
        }
    }

    public String getIdentifiant(){
        return this.identifiant.getText();
    }

    public String getMotDePasse() throws SQLException{
        return this.mdp.getText();
    }

    public String getPseudoInscription(){
        return this.pseudoInscription.getText();
    }

    public String getMailInscription(){
        return this.mailInscription.getText();
    }

    public String getMdpInscription(){
        return this.mdpInscription.getText();
    }

    public String getMailRecuperationMDP(){
        return this.mailRecuperationMDP.getText();
    }

    public void pageConnexion(){
        if(this.grandePage){
            this.stage.close();
            Pane root = new FenetreAccueil(this.mdpOublie, this.boutonInscription, this.boutonConnexion, this.identifiant, this.mdp, this.mauvaiseConnexion);
            this.scene = new Scene(root, 500, 500);
            this.stage.setTitle("Page connexion");
            this.stage.setScene(scene);
            this.stage.setResizable(false);
            this.stage.setMaximized(false);
            this.stage.show();
        }
        else{
            Pane root = new FenetreAccueil(this.mdpOublie, this.boutonInscription, this.boutonConnexion, this.identifiant, this.mdp, this.mauvaiseConnexion);
            this.scene.setRoot(root);
            this.stage.setTitle("Page connexion");
        }
    }

    public void pageMotDePasseOublie(){
        Pane root = new FenetreMotDePasseOublie(this.boutonRetourMenuConnexion, this.mailRecuperationMDP, this.boutonRecuperationMDP, this.mauvaisMailRecuperationMDP);
        this.scene.setRoot(root);
        this.stage.setTitle("Page mot de passe oublié");
    }

    public void pageInscription(){
        Pane root = new FenetreInscription(this.mdpAdministrateurInscription, this.mauvaisPseudo, this.mauvaisMail, this.mauvaisMdp, this.roleInscription, this.pseudoInscription, this.mailInscription, this.mdpInscription, this.boutonFinalisationInscription, this.boutonRetourMenuConnexion);
        this.stage.setTitle("Page inscription");
        this.scene.setRoot(root);
    }

    public void pageAcceuilUtilisateur(){
        if(this.grandePage){
            Pane root = new FenetreUser(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter, this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu, this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
            this.scene.setRoot(root);
            this.stage.setTitle("Page acceuil utilisateur");
        }
        else{
            this.stage.close();
            this.identifiant.clear();
            this.mdp.clear();
            Pane root = new FenetreUser(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter, this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
            this.scene = new Scene(root, 800, 500);
            this.stage.setTitle("Page acceuil utilisateur");
            this.stage.setScene(scene);
            this.stage.setResizable(true);
            this.stage.setMaximized(true);
            this.stage.show();
        }
    }

    public void pageAcceuilAdministrateur(){
        if(this.grandePage){
            Pane root = new FenetreAdmin(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
            this.scene.setRoot(root);
            this.stage.setTitle("Page acceuil administrateur");
        }
        else{
            this.stage.close();
            this.identifiant.clear();
            this.mdp.clear();
            Pane root = new FenetreAdmin(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
            this.scene = new Scene(root, 800, 500);
            this.stage.setTitle("Page acceuil administrateur");
            this.stage.setScene(scene);
            this.stage.setResizable(true);
            this.stage.setMaximized(true);
            this.stage.show();
        }
    } 

    public void setGrandPage(boolean val){
        this.grandePage = val;
    }
    
    public void pageMessage(){
        Pane root = new FenetreMessage(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page message");
    }  

    public void pageInfo(){
        Pane root = new FenetreInfo(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page information");
    } 

    public void pageEncherir(VBox enchere){
        Pane root = new FenetreEncherir(enchere, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page encherir");
    } 

    public void pageSetting(){
        Pane root = new FenetreSetting(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page paramètre");
    } 

    public void pageProfil(){
        Pane root = new FenetreProfil(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page profil");
    } 

    public void pageEnchere(){
        Pane root = new FenetreEnchere(this, this.venteModele, this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page enchère");
    } 

    public void pagePanier(){
        Pane root = new FenetrePanier(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page panier");
    } 

    public void pageAjouterVente(){
        Pane root = new FenetreAjouterVente(this.borderMenuOption, this.textMenu, this.textPageAccueil, this.textVendreUnProduit, this.textVenteAuxEncheres, this.textMessagerie, this.textMonPanier, this.textInformations, this.textSetting, this.textQuitter,  this.boutonQuitter, this.boutonDeconnexion, this.boutonMenu,  this.boutonAccueil, this.boutonMessage, this.boutonInfo, this.boutonSetting, this.boutonProfil, this.boutonEnchere, this.boutonPanier, this.boutonAjouterVente);
        this.scene.setRoot(root);
        this.stage.setTitle("Page ajouter vente");
    }
    
    public void popUpQuitterApplication(){ 
        // Création de la fenêtre de dialogue Alert
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Quitter l'application");
        alert.setHeaderText("Vous êtes sur de vouloir quitter l'application ?");
        
        // Appliquer un style CSS pour changer la couleur de fond de l'Alert
        alert.getDialogPane().setBackground(new Background(new BackgroundFill(Color.web("#012345"), null, null)));

        // Définition des boutons de la fenêtre de dialogue
        ButtonType yesButton = new ButtonType("Oui");
        ButtonType noButton = new ButtonType("Non");

        alert.getButtonTypes().setAll(yesButton, noButton);

        // Attente de la réponse de l'utilisateur
        javafx.stage.Window window = alert.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(event -> window.hide());

        alert.showAndWait().ifPresent(buttonType -> {
            if (buttonType == yesButton) {
                Platform.exit();
            } else if (buttonType == noButton) {
                alert.close();
            }
        });
    }

}