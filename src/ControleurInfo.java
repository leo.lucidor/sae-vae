import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurInfo implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurInfo(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageInfo();
    }
}