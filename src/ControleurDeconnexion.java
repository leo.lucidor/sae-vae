import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurDeconnexion implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurDeconnexion(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageConnexion();
        this.appli.setGrandPage(false);
    }
}