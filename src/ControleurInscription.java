import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurInscription implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    private Utilisateur utilisateur;
    private ComboBox<String> role;
    private PasswordField mdpAdminInscription;
    
    public ControleurInscription(ApplicationVAE appli, PasswordField mdpAdminInsciption, Utilisateur utilisateur, ComboBox<String> role){
        this.appli = appli;
        this.utilisateur = utilisateur;
        this.role = role;
        this.mdpAdminInscription = mdpAdminInsciption;
    }

    @Override
    public void handle(ActionEvent event) {
        String roleText = this.role.getValue();
        String pseudo = this.appli.getPseudoInscription();
        String mail = this.appli.getMailInscription();
        String mdp = this.appli.getMdpInscription();
        if(PseudoValidator.isValidPseudo(pseudo)){
            if(EmailValidator.isValidEmail(mail)){
                if(PasswordValidator.isValidPassword(mdp)){
                    if(roleText.equals("Administrateur")){
                        System.out.println("insertion administrateur");
                        try {
                            if(this.mdpAdminInscription.getText().equals("admin")){
                                this.utilisateur.insertUtilisateur(pseudo, mail, mdp, 1);
                                this.appli.pageAcceuilAdministrateur();
                                this.appli.setGrandPage(true);
                            }
                        } catch (SQLException e) {
                            System.out.println("Erreur lors de l'insertion de l'utilisateur");
                        }
                    }
                    else {
                        System.out.println("insertion utilisateur");
                        try {
                            this.utilisateur.insertUtilisateur(pseudo, mail, mdp, 2);
                            this.appli.pageAcceuilUtilisateur();
                            this.appli.setGrandPage(true);
                        } catch (SQLException e) {
                            System.out.println("Erreur lors de l'insertion de l'utilisateur");
                        }
                    }
                }
                else {
                    this.appli.changerVisibiliteMdp();
                    System.out.println("mot de passe non valide (1 caractere spécial, 8 caractere, 1 chiffre, 1 majuscule)");
                }
            }
            else {
                this.appli.changerVisibiliteMail();
                System.out.println("email non valide");
            }
        }
        else {
            this.appli.changerVisibilitePseudo();
            System.out.println("pseudo trop court minimum 3 caractere");
        }
    }
}
