import javafx.event.EventHandler;
import java.sql.SQLException;
import javafx.event.ActionEvent;

public class ControleurChoixTypeUserAdmin implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    private Utilisateur utilisateur;
    
    public ControleurChoixTypeUserAdmin(ApplicationVAE appli, Utilisateur utilisateur){
        this.appli = appli;
        this.utilisateur = utilisateur;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            String pseudo = this.appli.getIdentifiant();
            String tenterMdp = this.appli.getMotDePasse();
            String mdp = this.utilisateur.getMdp(pseudo);
            int id = this.utilisateur.getId(pseudo);
            String role = this.utilisateur.getRole(id);
            if(mdp.equals(tenterMdp)){
                if(role.equals("1")){
                    this.appli.pageAcceuilAdministrateur();
                    this.appli.setGrandPage(true);
                    this.utilisateur.setRole(1);
                }
                else {
                    this.appli.pageAcceuilUtilisateur();
                    this.appli.setGrandPage(true);
                    this.utilisateur.setRole(2);
                }
            }
            else{
                this.appli.changerVisibiliteConnexion();
                System.out.println("le pseudo ou mdp est incorrect");
            }
        } catch (SQLException e) {
            System.out.println("le pseudo ou mdp est incorrect");
            this.appli.changerVisibiliteConnexion();
        }
    }
}
