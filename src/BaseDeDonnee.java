import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.SQLException;

public class BaseDeDonnee {
    Connection connection = null;

    public BaseDeDonnee(String repertoire) throws SQLException{
        File file = new File(repertoire);
        System.out.println(file.toURI().toString());
        this.connection = DriverManager.getConnection( "jdbc:sqlite:" + file.toURI().toString()  );
        // creation();
    }

    public void creation(){
        try {
            // Chargement du pilote JDBC SQLite
            Class.forName("org.sqlite.JDBC");

            // Création de la base de données et des tables
            Statement statement = this.connection.createStatement();

            // Création des tables
            String createTableQuery = creationBD("./bd/creation_vae.sql");
            statement.executeUpdate(createTableQuery);
            statement.close();
            // Insertion du jeu de donnés
            statement = this.connection.createStatement();
            String insertDataQuery = creationBD("./bd/jeu_essai_vae.sql");
            statement.executeUpdate(insertDataQuery);
            statement.close();
        }
        catch (ClassNotFoundException e) {
            System.err.println("Impossible de charger le pilote JDBC SQLite");
        } 
        catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données SQLite");
            e.printStackTrace();
        } 
        finally {
            // Fermeture de la connexion
            if (this.connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                    // Gestion des exceptions
                }
            }
        }
    }

    int maxNum(String table, String id) throws SQLException{
		Statement st = this.connection.createStatement();
		//execution de la requete
		ResultSet rs = st.executeQuery("select max(" + id + ") from " + table);
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		int res  =rs.getInt(1);
		rs.close();
		return res;
	}

    public void insertUtilisateur(String nom, String mail, String mdp, String active, int role) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("insert into UTILISATEUR(idUt,pseudoUt,emailUT,mdpUt,activeUt,idRole) values (?, ?, ?, ?, ?, ?)");
        ps.setInt(1, maxNum("UTILISATEUR", "idUt") + 1);
        ps.setString(2, nom);
        ps.setString(3, mail);
        ps.setString(4, mdp);
        ps.setString(5, active);
        ps.setInt(6, role);
        ps.executeUpdate();
    }

    public void insertObjet(String nom, String description, int prix, int prixMin, int categorie, int idUtilisateur) throws SQLException{
        PreparedStatement ps = this.connection.prepareStatement("insert into OBJET(idOb,nomOb,descriptionOb,prixOb,idCat,idUt) values (?, ?, ?, ?, ?, ?)");
        ps.setInt(1, maxNum("OBJET", "idOb") + 1);
        ps.setString(2, nom);
        ps.setString(3, description);
        ps.setInt(4, prix);
        ps.setInt(5, categorie);
        ps.setInt(6, idUtilisateur);
        ps.executeUpdate();
        ps = this.connection.prepareStatement("insert into VENTE(idOb,prixMin) values (?, ?)");
        ps.setInt(1, maxNum("OBJET", "idOb"));
        ps.setInt(2, prixMin);
        ps.executeUpdate();
    }

    private String creationBD(String filePath){
        try{
          // Le fichier d'entrée
            File file = new File(filePath);    
            // Créer l'objet File Reader
            FileReader fr = new FileReader(file);  
            // Créer l'objet BufferedReader        
            BufferedReader br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();    
            String line;
            while((line = br.readLine()) != null){
                // ajoute la ligne au buffer
                sb.append(line);      
            }
            fr.close();    
            return sb.toString();
        }
        catch(IOException e){
            e.printStackTrace();
            return "";
        }
    }

    public int getId(String pseudo) throws SQLException{
        // création d'un prepared statement pour recuperer le mdp
        PreparedStatement ps = this.connection.prepareStatement("select idut from UTILISATEUR where pseudoUt = ?");
		ps.setString(1, pseudo);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		int res  = rs.getInt(1);
		rs.close();
		return res;
    }

    public String getMdpPseudo(String pseudo) throws SQLException{
        // création d'un prepared statement pour recuperer le mdp
        PreparedStatement ps = this.connection.prepareStatement("select mdpUt from UTILISATEUR where pseudoUt = ?");
		ps.setString(1, pseudo);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		String res  =rs.getString(1);
		rs.close();
		return res;
    }

    public String getMdp(int id) throws SQLException{
        // création d'un prepared statement pour recuperer le mdp
        PreparedStatement ps = this.connection.prepareStatement("select mdpUt from UTILISATEUR where idut = ?");
		ps.setInt(1, id);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		String res  =rs.getString(1);
		rs.close();
		return res;
    }

    public String getMdpMail(String email) throws SQLException{
        // création d'un prepared statement pour recuperer le mdp
        PreparedStatement ps = this.connection.prepareStatement("select mdpUt from UTILISATEUR where emailut = ?");
		ps.setString(1, email);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		String res  =rs.getString(1);
		rs.close();
		return res;
    }

    public String getRole(int id) throws SQLException{
        // création d'un prepared statement pour recuperer le role
        PreparedStatement ps = this.connection.prepareStatement("select idrole from UTILISATEUR where idut = ?");
		ps.setInt(1, id);
        //execution de la requete
		ResultSet rs = ps.executeQuery();
		//chargement de la 1ere ligne
		rs.next();
		//consultation de la ligne
		String res  =rs.getString(1);
		rs.close();
		return res;
    }


    public List<List<String>> getVente(String nomOb, String nomVille) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        // création d'un prepared statement pour recuperer les ventes
        PreparedStatement ps = this.connection.prepareStatement("select pseudout, nomob, prixbase, prixmin, descriptionOb, nomville from VENTE natural join OBJET natural join UTILISATEUR where nomob like ? and nomville like ?");
        ps.setString(1, nomOb + '%');
        ps.setString(2, nomVille + '%');
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        //chargement de la 1ere ligne
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(rs.getString(1));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(rs.getString(5));
            ligne.add(rs.getString(6));
            res.add(ligne);
        }
        //consultation de la ligne
        rs.close();
        return res;
    }

    public List<List<String>> getVenteAvecCategorie(String nomOb, String nomVille, int categorie) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        // création d'un prepared statement pour recuperer les ventes
        PreparedStatement ps = this.connection.prepareStatement("select pseudout, nomob, prixbase, prixmin, descriptionOb, nomville from VENTE natural join OBJET natural join UTILISATEUR where nomob like ? and idcat = ? and nomville like ?");
        ps.setString(1, nomOb + '%');
        ps.setInt(2, categorie);
        ps.setString(3, nomVille + '%');
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        //chargement de la 1ere ligne
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(rs.getString(1));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(rs.getString(5));
            ligne.add(rs.getString(6));
            res.add(ligne);
        }
        //consultation de la ligne
        rs.close();
        return res;
    }

    public List<List<String>> getVenteAvecPrixCroissant(String nomOb, String nomVille, int prixMax) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        // création d'un prepared statement pour recuperer les ventes
        PreparedStatement ps = this.connection.prepareStatement("select pseudout, nomob, prixbase, prixmin, descriptionOb, nomville from VENTE natural join OBJET natural join UTILISATEUR where nomob like ? and nomville like ? and prixmin <= ? order by prixmin");
        ps.setString(1, nomOb + '%');
        ps.setString(2, nomVille + '%');
        ps.setInt(3, prixMax);
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        //chargement de la 1ere ligne
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(rs.getString(1));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(rs.getString(5));
            ligne.add(rs.getString(6));
            res.add(ligne);
        }
        //consultation de la ligne
        rs.close();
        return res;
    }

    public List<List<String>> getVenteAvecPrixDecroissant(String nomOb, String nomVille, int prixMax) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        // création d'un prepared statement pour recuperer les ventes
        PreparedStatement ps = this.connection.prepareStatement("select pseudout, nomob, prixbase, prixmin, descriptionOb, nomville from VENTE natural join OBJET natural join UTILISATEUR where nomob like ? and nomville like ? and prixmin <= ? order by prixmin DESC");
        ps.setString(1, nomOb + '%');
        ps.setString(2, nomVille + '%');
        ps.setInt(3, prixMax);
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        //chargement de la 1ere ligne
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(rs.getString(1));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(rs.getString(5));
            ligne.add(rs.getString(6));
            res.add(ligne);
        }
        //consultation de la ligne
        rs.close();
        return res;
    }

    public List<List<String>> getVenteAvecCategorieEtPrixCroissant(String nomOb, String nomVille, int categorie, int prixMax) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        // création d'un prepared statement pour recuperer les ventes
        PreparedStatement ps = this.connection.prepareStatement("select pseudout, nomob, prixbase, prixmin, descriptionOb, nomville from VENTE natural join OBJET natural join UTILISATEUR where nomob like ? and idcat = ? and nomville like ? and prixmin <= ? order by prixmin");
        ps.setString(1, nomOb + '%');
        ps.setInt(2, categorie);
        ps.setString(3, nomVille + '%');
        ps.setInt(4, prixMax);
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        //chargement de la 1ere ligne
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(rs.getString(1));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(rs.getString(5));
            ligne.add(rs.getString(6));
            res.add(ligne);
        }
        //consultation de la ligne
        rs.close();
        return res;
    }

    public List<List<String>> getVenteAvecCategorieEtPrixDecroissant(String nomOb, String nomVille, int categorie, int prixMax) throws SQLException{
        List<List<String>> res = new ArrayList<>();
        // création d'un prepared statement pour recuperer les ventes
        PreparedStatement ps = this.connection.prepareStatement("select pseudout, nomob, prixbase, prixmin, descriptionOb, nomville from VENTE natural join OBJET natural join UTILISATEUR where nomob like ? and idcat = ? and nomville like ? and prixmin <= ? order by prixmin DESC");
        ps.setString(1, nomOb + '%');
        ps.setInt(2, categorie);
        ps.setString(3, nomVille + '%');
        ps.setInt(4, prixMax);
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        //chargement de la 1ere ligne
        while (rs.next()){
            List<String> ligne = new ArrayList<>();
            ligne.add(rs.getString(1));
            ligne.add(rs.getString(2));
            ligne.add(Integer.toString(rs.getInt(3)));
            ligne.add(Integer.toString(rs.getInt(4)));
            ligne.add(rs.getString(5));
            ligne.add(rs.getString(6));
            res.add(ligne);
        }
        //consultation de la ligne
        rs.close();
        return res;
    }


    public List<String> getVenteUtilisateur(String nom) throws SQLException{
        List<String> res = new ArrayList<String>();
        // création d'un prepared statement pour recuperer les ventes
        PreparedStatement ps = this.connection.prepareStatement("select pseudout, nomob, prixbase, prixmin from VENTE natural join OBJET natural join UTILISATEUR where pseudoUt = ?");
        ps.setString(1, nom + '%');
        //execution de la requete
        ResultSet rs = ps.executeQuery();
        //chargement de la 1ere ligne
        while (rs.next()){
            String ligne = "";
            ligne += rs.getString(1) + ", ";
            ligne += rs.getString(2) + ", ";
            ligne += rs.getInt(3) + ", ";
            ligne += rs.getInt(4);
            res.add(ligne);
        }
        //consultation de la ligne
        rs.close();
        return res;
    }

    public void reset(){
        String reset;
        try{
            // Le fichier d'entrée
            File file = new File("./bd/destruction_vae.sql");    
              // Créer l'objet File Reader
            FileReader fr = new FileReader(file);  
              // Créer l'objet BufferedReader        
            BufferedReader br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();    
            String line;
            while((line = br.readLine()) != null){
                // ajoute la ligne au buffer
                sb.append(line);      
            }
            reset = sb.toString();
            fr.close();
            // Chargement du pilote JDBC SQLite
            Class.forName("org.sqlite.JDBC");
            Statement statement = this.connection.createStatement();
            statement.executeUpdate(reset);
            statement.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            System.err.println("Impossible de charger le pilote JDBC SQLite");
        } 
        catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données SQLite");
            e.printStackTrace();
        }
    }
}
