import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurProfil implements EventHandler<ActionEvent> {
    
    private ApplicationVAE appli;
    
    public ControleurProfil(ApplicationVAE appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.pageProfil();
    }
}